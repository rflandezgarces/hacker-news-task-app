var axios = require("axios");
var Helpers = require("../commons/Helpers");
const Article = require("../models/article");

//Get a single Article from db by Id
exports.getById = async (id) => {
  try {
    let result = await Article.findById(id);
    return result;
  } catch (err) {
    return err;
  }
};

//Get the last Article stored in db
exports.getLast = async () => {
  try {
    let result = await Article.findOne().sort("-created_at");
    return result;
  } catch (err) {
    return err;
  }
};

//Get all Articles from db
exports.getAll = async () => {
  try {
    let result = await Article.find({ deleted: false }).sort({ date: "desc" });
    return result;
  } catch (err) {
    return err;
  }
};

//Get Articles from Hacker News
exports.getFromHackerNews = async () => {
  try {
    let lastArticle = await this.getLast();
    let lastDate = lastArticle ? lastArticle.date : 0;
    let result = await axios.get(
      `${process.env.HACKER_NEWS_API_URL}/search_by_date?query=nodejs&numericFilters=created_at_i>${lastDate}`
    );
    return result;
  } catch (err) {
    return err;
  }
};

//Insert a collection of Article in db from Hacker News
exports.createFromHackerNews = async () => {
  try {
    let data = await this.getFromHackerNews();
    if (data.data.hits.length > 0) {
      let articles = Helpers.hnToArticleModel(data.data.hits);
      await this.createMany(articles);
    }
    return true;
  } catch (err) {
    return err;
  }
};

//Insert a new Article in db
exports.createNew = async (article) => {
  try {
    let result = await Article.create(article);
    return result;
  } catch (err) {
    return err;
  }
};

//Insert a collection of Article in db
exports.createMany = async (articles) => {
  try {
    let result = await Article.insertMany(articles);
    return result;
  } catch (err) {
    return err;
  }
};

//Update a single Article from db by Id
exports.updateById = async (id, updates) => {
  try {
    let result = await Article.findByIdAndUpdate(id, updates);
    return result;
  } catch (err) {
    return err;
  }
};

//Delete a single Article from db by Id
exports.deleteById = async (id) => {
  try {
    let result = await this.updateById(id, { deleted: true });
    return result;
  } catch (err) {
    return err;
  }
};
