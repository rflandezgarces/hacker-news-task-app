var express = require("express");
var router = express.Router();

// Require controller modules.
var articlesController = require("../controllers/articlesController");

/// ARTICLE ROUTES ///

// GET request for list of all Article items.
router.get("/all", articlesController.getAll);

// DELETE request Article item by id.
router.delete("/:id", articlesController.deleteById);

module.exports = router;
