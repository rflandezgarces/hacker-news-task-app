const ArticleService = require("../services/articleService");

// Display list of all Article.
exports.getAll = async (req, res) => {
  try {
    let result = await ArticleService.getAll();
    res.status(200).send({ result: true, data: result });
  } catch (err) {
    res.status(500).send({ result: false, data: err });
  }
};

// Delete Article by id.
exports.deleteById = async (req, res) => {
  let articleId = req.params.id;
  try {
    let result = await ArticleService.deleteById(articleId);
    res.status(200).send({ result: true, data: result });
  } catch (err) {
    res.status(500).send({ result: false, data: err });
  }
};
