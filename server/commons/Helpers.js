exports.hnToArticleModel = (hnJson) => {
  let articles = [];
  hnJson.forEach((element) => {
    if (
      (element.story_title || element.title) &&
      (element.story_url || element.url)
    ) {
      articles = [
        ...articles,
        {
          objectId: element.objectID,
          title: element.story_title ? element.story_title : element.title,
          url: element.story_url ? element.story_url : element.url,
          author: element.author,
          date: element.created_at_i,
        },
      ];
    }
  });

  return articles;
};
