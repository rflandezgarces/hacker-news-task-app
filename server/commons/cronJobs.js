const cron = require("node-cron");
const ArticleService = require("../services/articleService");

exports.getArticlesEveryHour = async () => {
  try {
    await ArticleService.createFromHackerNews();
  } catch (err) {}
  cron.schedule("0 */1 * * *", async () => {
    try {
      await ArticleService.createFromHackerNews();
    } catch (err) {}
  });
};
