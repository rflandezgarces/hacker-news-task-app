exports.getArticleObj = () => {
  return {
    objectId: "666",
    title: "Mira este articulo de Hacker News",
    url: "https://www.fayerwayer.com/",
    author: "buen_tipo",
    deleted: false,
    date: +new Date(),
  };
};

exports.getManyArticles = () => {
  return [
    {
      objectId: "666",
      title: "Mira este primer articulo de Hacker News",
      url: "https://www.fayerwayer.com/",
      author: "buen_tipo_1",
      deleted: false,
      date: +new Date(),
    },
    {
      objectId: "667",
      title: "Mira este segundo articulo de Hacker News",
      url: "https://www.fayerwayer.com/",
      author: "buen_tipo_2",
      deleted: false,
      date: +new Date(),
    },
    {
      objectId: "668",
      title: "Mira este tercer articulo de Hacker News",
      url: "https://www.fayerwayer.com/",
      author: "buen_tipo_3",
      deleted: false,
      date: +new Date(),
    },
  ];
};
