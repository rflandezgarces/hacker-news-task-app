var express = require("express");
var cors = require("cors");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
require("dotenv").config();
require("./config/database");
var cronJobs = require("./commons/cronJobs");

var articlesRouter = require("./routes/articles");

const CLIENT_BUILD_PATH = path.join(__dirname, "../client/build/");

var app = express();

cronJobs.getArticlesEveryHour();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());
app.use(express.static(CLIENT_BUILD_PATH));

app.use("/api/articles", articlesRouter);

app.get("/", function (req, res) {
  res.sendFile(path.join(CLIENT_BUILD_PATH, "index.html"));
});

if (process.env.NODE_ENV !== "test") app.listen(process.env.PORT || 3000);

module.exports = app;
