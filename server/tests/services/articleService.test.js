const assert = require("chai").assert;
const ArticleService = require("../../services/articleService");
const testObjects = require("../../commons/testObjects");

describe("Article Service", () => {
  describe("Get Article By id", () => {
    it("Exposes the getById  method", () => {
      assert.isFunction(ArticleService.getById);
    });
  });

  describe("Get last article", () => {
    it("Exposes the getLast method", () => {
      assert.isFunction(ArticleService.getLast);
    });
  });

  describe("Get all articles", () => {
    it("Exposes the getAll method", () => {
      assert.isFunction(ArticleService.getAll);
    });
  });

  describe("Get all articles from hacker news", () => {
    it("Exposes the getFromHackerNews method", () => {
      assert.isFunction(ArticleService.getFromHackerNews);
    });
  });

  describe("Create many articles from hacker news", () => {
    it("Exposes the createFromHackerNews method", () => {
      assert.isFunction(ArticleService.createFromHackerNews);
    });
  });

  describe("Create new article", () => {
    it("Exposes the createNew method", () => {
      assert.isFunction(ArticleService.createNew);
    });
    /* it("should create a new article", () => {
      let article = testObjectsgetArticleObj();
      ArticleService.createNew(article).then((newArticle) => {
        assert.equal(newArticle.objectId, article.objectId);
        newArticle.should.be.a("object");
      });
    }); */
  });

  describe("Create many articles", () => {
    it("Exposes the createMany method", () => {
      assert.isFunction(ArticleService.createMany);
    });
    /* it("should create many new articles", async () => {
      let manyArticles = testObjects.getManyArticles();
      ArticleService.createMany(manyArticles).then((manyNewArticles) => {
        assert.equal(manyNewArticles.length, manyArticles.length);
        assert.equal(manyNewArticles[0].objectId, manyArticles[0].objectId);
        assert.equal(manyNewArticles[1].objectId, manyArticles[1].objectId);
        assert.equal(manyNewArticles[2].objectId, manyArticles[2].objectId);
        newArticle.should.be.a("object");
      });
    }); */
  });

  describe("Update article by id", () => {
    it("Exposes the updateById method", () => {
      assert.isFunction(ArticleService.updateById);
    });
  });

  describe("Delete article by id", () => {
    it("Exposes the deleteById method", () => {
      assert.isFunction(ArticleService.deleteById);
    });
  });
});
