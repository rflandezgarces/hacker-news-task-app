const mongoose = require("mongoose");

const options = {
  useNewUrlParser: true,
  useFindAndModify: false,
  useCreateIndex: true,
  useUnifiedTopology: true,
  autoIndex: false,
  poolSize: 10,
  bufferMaxEntries: 0,
};

let nodeEnv = process.env.NODE_ENV;
const dbConnectionURL = {
  LOCALURL: `mongodb://${process.env.MONGO_HOSTNAME}:${
    process.env.MONGO_PORT
  }/${nodeEnv === "test" ? process.env.MONGO_DB_TEST : process.env.MONGO_DB}`,
};

mongoose.connect(dbConnectionURL.LOCALURL, options);
const db = mongoose.connection;
db.on(
  "error",
  console.error.bind(
    console,
    "Mongodb Connection Error:" + dbConnectionURL.LOCALURL
  )
);
db.once("open", () => {});
