import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ArticleListElement from "./ArticleListElement";
import ListIsEmpty from "./ListIsEmpty";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
  },
}));

const ArticleList = (props) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      {props.elements.length > 0 ? (
        <List className={classes.root}>
          {props.elements.map((element, key) => (
            <ArticleListElement
              key={key}
              element={element}
              deleteArticleById={props.deleteArticleById}
            />
          ))}
        </List>
      ) : (
        <ListIsEmpty />
      )}
    </React.Fragment>
  );
};

export default ArticleList;
