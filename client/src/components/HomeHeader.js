import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    height: 200,
    backgroundColor: theme.palette.primary.dark,
    justifyContent: "center",
    padding: 30,
  },
  icon: {
    color: theme.palette.error.main,
    marginLeft: 5,
    marginRight: 5,
    marginTop: -5,
  },
  title: {
    color: theme.palette.secondary.light,
  },
  subtitleContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
  },
  subtitle: {
    color: theme.palette.primary.light,
  },
}));

const HomeHeader = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Typography className={classes.title} variant="h4" component="h1">
        HN Feed
      </Typography>
      <div className={classes.subtitleContainer}>
        <Typography className={classes.subtitle} variant="subtitle1">
          We
        </Typography>
        <FavoriteIcon className={classes.icon} />
        <Typography className={classes.subtitle} variant="subtitle1">
          Hacker News !!!
        </Typography>
      </div>
    </div>
  );
};

export default HomeHeader;
