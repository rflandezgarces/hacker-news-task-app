import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import MoodBadIcon from "@material-ui/icons/MoodBad";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    marginTop: 50,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  iconEmpty: {
    width: 50,
    height: 50,
    marginBottom: 10,
    color: theme.palette.primary.main,
  },
}));

const ListIsEmpty = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <MoodBadIcon className={classes.iconEmpty} />
      <Typography variant="body1">This List is Empty !!!</Typography>
    </div>
  );
};

export default ListIsEmpty;
