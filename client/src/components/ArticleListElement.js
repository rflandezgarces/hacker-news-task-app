import React from "react";
import { withStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import Divider from "@material-ui/core/Divider";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import Typography from "@material-ui/core/Typography";
import * as helpers from "../commons/helpers";

const styles = (theme) => ({
  listItem: {
    "&:hover $secondaryAction": {
      visibility: "visible",
    },
  },
  listItemText: {
    width: "70%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  authorName: {
    marginLeft: 10,
  },
  secondaryAction: {
    visibility: "hidden",
  },
  hourContainer: {
    width: "30%",
  },
  hour: {
    marginRight: 70,
    marginLeft: 40,
  },
});

const ArticleListElement = (props) => {
  const { classes } = props;

  const onClickListElement = (url) => {
    window.open(url, "_blank");
  };

  return (
    <React.Fragment>
      <ListItem
        classes={{ container: classes.listItem }}
        onClick={(e) => onClickListElement(props.element.url)}
        button
      >
        <ListItemText
          classes={{ secondary: classes.authorName }}
          className={classes.listItemText}
          primary={props.element.title}
          secondary={` - ${props.element.author}`}
        />
        <div className={classes.hourContainer}>
          <Typography className={classes.hour} variant="subtitle1">
            {helpers.timestampFormater(props.element.date)}
          </Typography>
        </div>
        <ListItemSecondaryAction className={classes.secondaryAction}>
          <IconButton
            aria-label="delete"
            onClick={(e) => props.deleteArticleById(props.element._id)}
          >
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
      <Divider variant="middle" component="li" />
    </React.Fragment>
  );
};

export default withStyles(styles)(ArticleListElement);
