import React, { useState, useEffect, useCallback } from "react";
import HomeHeader from "../components/HomeHeader";
import ArticleList from "../components/ArticleList";
import * as articleService from "../services/articleService";

const Home = () => {
  const [articles, setArticles] = React.useState([]);

  useEffect(() => {
    getArticles();
  }, []);

  const getArticles = async () => {
    try {
      let data = await articleService.getAllArticles();
      setArticles(data.data.data);
    } catch (err) {}
  };

  const deleteArticleById = async (articleId) => {
    try {
      await articleService.deleteArticleById(articleId);
      setArticles((state) => state.filter((x) => x._id !== articleId));
    } catch (err) {}
  };

  return (
    <React.Fragment>
      <HomeHeader />
      <ArticleList elements={articles} deleteArticleById={deleteArticleById} />
    </React.Fragment>
  );
};

export default Home;
