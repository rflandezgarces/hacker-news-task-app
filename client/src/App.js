import React from "react";
import { HashRouter, Route, Switch } from "react-router-dom";
import CircularProgress from "@material-ui/core/CircularProgress";

const loading = () => <CircularProgress />;

// Pages
const Home = React.lazy(() => import("./views/Home"));

function App() {
  return (
    <HashRouter>
      <React.Suspense fallback={loading()}>
        <Switch>
          <Route
            exact
            path="/"
            name="Home"
            render={(props) => <Home {...props} />}
          />
        </Switch>
      </React.Suspense>
    </HashRouter>
  );
}

export default App;
