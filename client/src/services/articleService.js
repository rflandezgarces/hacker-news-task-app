import httpService from "./httpService";

export function getAllArticles() {
  return httpService.get(`/articles/all`);
}

export function deleteArticleById(articleId) {
  return httpService.delete(`/articles/${articleId}`);
}
