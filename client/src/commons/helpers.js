exports.timestampFormater = (timestamp) => {
  var actualDay = new Date().getDate();

  var date = new Date(timestamp * 1000);
  var day = date.getDate();
  var month = date.getMonth();
  var hours = date.getHours();
  var minutes = "0" + date.getMinutes();
  var ampm = hours >= 12 ? "pm" : "am";

  if (actualDay - 1 === day) {
    return "Yesterday";
  } else if (actualDay === day) {
    return `${hours}:${minutes.substr(-2)} ${ampm}`;
  } else {
    return `${("0" + day).substr(-2)} ${this.monthNames(month).substr(0, 3)}`;
  }
};

exports.monthNames = (month) => {
  const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  return monthNames[month];
};
