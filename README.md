## Hacker News App

### Prerequisites:

You must have Docker Installed in your System !

### How to run it:
Run the app using :

` $ docker-compose up --build --remove-orphans`

The App populate the database automatically the first time you run it. Then, once an hour it gets the articles again (only from the date of the last article stored).

The App should be up at http://localhost:8080